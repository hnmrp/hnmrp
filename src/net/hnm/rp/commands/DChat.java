package net.hnm.rp.commands;

import net.hnm.rp.factions.*;
import java.util.Iterator;
import net.hnm.rp.main.HeavenRolePlay;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DChat implements CommandExecutor {
    private HeavenRolePlay plugin;

    public DChat(HeavenRolePlay plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Команда может быть выполнена только игроком");
            return true;
        } else {
            Player p = (Player)sender;
            if (!this.plugin.getData().contains("players." + p.getName().toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Вы не находитесь в какой-либо фракции");
                return true;
            } else {
                String faction = this.plugin.getData().getString("players." + p.getName().toLowerCase()).split(" ")[0];
                if (Integer.parseInt(faction) > 7) {
                    p.sendMessage(ChatColor.RED + "У вашей фракции нет прав писать в этот чат");
                    return true;
                } else {
                    String rank = this.plugin.getData().getString("players." + p.getName().toLowerCase()).split(" ")[1];
                    StringBuilder message = new StringBuilder("");
                    String[] var12 = args;
                    int var11 = args.length;

                    for(int var10 = 0; var10 < var11; ++var10) {
                        String word = var12[var10];
                        message.append(word + " ");
                    }

                    Iterator var14 = Bukkit.getOnlinePlayers().iterator();

                    while(var14.hasNext()) {
                        Player player = (Player)var14.next();
                        if (SpyCommand.spy.contains(player)) {
                            player.sendMessage(ChatColor.GRAY + "[Шпион] " + ChatColor.RED + "[DepartRP] " + ChatColor.YELLOW + "[" + getFactionName(Integer.parseInt(faction)) + "]" + ChatColor.GREEN + " " + "[" + getRank(Integer.parseInt(faction), Integer.parseInt(rank)) + "]" + ChatColor.AQUA + " " + p.getName() + ChatColor.YELLOW + ": " + ChatColor.WHITE + message.toString());
                            return true;
                        }

                        if (Integer.parseInt(this.plugin.getData().getString("players." + player.getName().toLowerCase()).split(" ")[0]) < 8) {
                            player.sendMessage(ChatColor.RED + "[DepartRP] " + ChatColor.YELLOW + "[" + getFactionName(Integer.parseInt(faction)) + "]" + ChatColor.GREEN + " " + "[" + getRank(Integer.parseInt(faction), Integer.parseInt(rank)) + "]" + ChatColor.AQUA + " " + p.getName() + ChatColor.YELLOW + ": " + ChatColor.WHITE + message.toString());
                        }
                    }

                    return true;
                }
            }
        }
    }

    private static String getFactionName(int faction) {
        switch(faction) {
            case 0:
                return "ФСБ";
            case 1:
                return "ППС";
            case 2:
                return "АО";
            case 3:
                return "ГИБДД";
            case 4:
                return "Областная больница";
            case 5:
                return "СМИ";
            case 6:
                return "АШ";
            case 7:
                return "МЧС";
            case 8:
                return "Сепаратисты";
            case 9:
                return "Пираты";
            case 10:
                return "Батыревская бригада";
            default:
                return null;
        }
    }

    private static String getRank(int faction, int number) {
        switch(faction) {
            case 0:
                return (String)FCBFaction.ranks.get(number);
            case 1:
                return (String)PPSFaction.ranks.get(number);
            case 2:
                return (String)AOFaction.ranks.get(number);
            case 3:
                return (String)GIBDDFaction.ranks.get(number);
            case 4:
                return (String)OBFaction.ranks.get(number);
            case 5:
                return (String)SMIFaction.ranks.get(number);
            case 6:
                return (String)ASHFaction.ranks.get(number);
            case 7:
                return (String)MCHSFaction.ranks.get(number);
            case 8:
                return (String)SEPFaction.ranks.get(number);
            case 9:
                return (String)PIRFaction.ranks.get(number);
            case 10:
                return (String)BATFaction.ranks.get(number);
            default:
                return null;
        }
    }
}