package net.hnm.rp.commands;

import net.hnm.rp.factions.*;
import java.util.Iterator;
import net.hnm.rp.main.HeavenRolePlay;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InfoCommand implements CommandExecutor {
    private HeavenRolePlay plugin;

    public InfoCommand(HeavenRolePlay plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length != 1) {
            return false;
        } else {
            Player p = (Player)sender;
            if (!this.plugin.getData().contains("players." + args[0].toLowerCase())) {
                p.sendMessage(ChatColor.RED + "Нет информации об этом игроке");
                return true;
            } else {
                int faction = Integer.parseInt(this.plugin.getData().getString("players." + args[0].toLowerCase()).split(" ")[0]);
                int rank = Integer.parseInt(this.plugin.getData().getString("players." + args[0].toLowerCase()).split(" ")[1]);
                p.sendMessage(ChatColor.AQUA + "=============Faction info=================");
                p.sendMessage(ChatColor.GOLD + "Ник игрока: " + ChatColor.RED + args[0].toLowerCase());
                switch(faction) {
                    case 0:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + "ФСБ");
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)FCBFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 1:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + "ППС");
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)PPSFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 2:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + "АО");
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)AOFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 3:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + GIBDDFaction.FactionName);
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)GIBDDFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 4:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + "Областная больница");
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)OBFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 5:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + "СМИ");
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)SMIFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 6:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + ASHFaction.FactionName);
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)ASHFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 7:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + MCHSFaction.FactionName);
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)MCHSFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 8:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + SEPFaction.FactionName);
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)SEPFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 9:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + PIRFaction.FactionName);
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)PIRFaction.ranks.get(rank) + "(" + rank + ")");
                        break;
                    case 10:
                        p.sendMessage(ChatColor.GOLD + "Фракция игрока: " + ChatColor.RED + "Батыревская бригада");
                        p.sendMessage(ChatColor.GOLD + "Ранг игрока: " + ChatColor.RED + (String)BATFaction.ranks.get(rank) + "(" + rank + ")");
                }

                p.sendMessage(ChatColor.AQUA + "=========================================");
                return true;
            }
        }
    }
}