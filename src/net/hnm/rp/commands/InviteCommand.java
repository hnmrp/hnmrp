package net.hnm.rp.commands;

import net.hnm.rp.factions.*;
import net.hnm.rp.main.HeavenRolePlay;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InviteCommand implements CommandExecutor {
    private HeavenRolePlay plugin;

    public InviteCommand(HeavenRolePlay plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length != 0 && args.length <= 2) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Команда может быть выполнена только игроком");
                return true;
            } else {
                Player p = (Player)sender;
                String faction;
                if (args.length == 1) {
                    if (!this.plugin.getData().contains("players." + p.getName().toLowerCase())) {
                        sender.sendMessage(ChatColor.RED + "Вы не находитесь в какой-либо фракции");
                        return true;
                    } else if (this.plugin.getData().contains("players." + args[0].toLowerCase())) {
                        sender.sendMessage(ChatColor.RED + "Игрок уже находится в какой-либо фракции");
                        return true;
                    } else {
                        faction = this.plugin.getData().getString("players." + p.getName().toLowerCase()).split(" ")[0];
                        String rank = this.plugin.getData().getString("players." + p.getName().toLowerCase()).split(" ")[1];
                        switch(faction.hashCode()) {
                            case 48:
                                if (faction.equals("0")) {
                                    if (!rank.equals("8") && !rank.equals("9")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    FCBFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 49:
                                if (faction.equals("1")) {
                                    if (!rank.equals("8") && !rank.equals("9")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    PPSFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 50:
                                if (faction.equals("2")) {
                                    if (!rank.equals("8") && !rank.equals("9")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    AOFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 51:
                                if (faction.equals("3")) {
                                    if (!rank.equals("8") && !rank.equals("9")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    GIBDDFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 52:
                                if (faction.equals("4")) {
                                    if (!rank.equals("8") && !rank.equals("9")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    OBFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 53:
                                if (faction.equals("5")) {
                                    if (!rank.equals("8") && !rank.equals("9")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    SMIFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 54:
                                if (faction.equals("6")) {
                                    if (!rank.equals("6") && !rank.equals("7")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    ASHFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 55:
                                if (faction.equals("7")) {
                                    if (!rank.equals("7") && !rank.equals("8")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    MCHSFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 56:
                                if (faction.equals("8")) {
                                    if (!rank.equals("10") && !rank.equals("11")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    SEPFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 57:
                                if (faction.equals("9")) {
                                    if (!rank.equals("8") && !rank.equals("9")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    PIRFaction.invite(args[0].toLowerCase());
                                }
                                break;
                            case 1567:
                                if (faction.equals("10")) {
                                    if (!rank.equals("8") && !rank.equals("9")) {
                                        sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для приглашения новых игроков");
                                        return true;
                                    }

                                    BATFaction.invite(args[0].toLowerCase());
                                }
                        }

                        sender.sendMessage(ChatColor.GREEN + "Игрок успешно приглашен во фракцию");
                        return true;
                    }
                } else if (this.plugin.getData().contains("players." + args[1].toLowerCase())) {
                    sender.sendMessage(ChatColor.RED + "Игрок уже находится в какой-либо фракции");
                    return true;
                } else if (!p.hasPermission("HeavenFactions.pfinvite")) {
                    p.sendMessage(ChatColor.RED + "У вас нет прав добавлять игроков во фракции");
                    return true;
                } else {
                    try {
                        int a = Integer.parseInt(args[0]);
                        if (a < 0 || a > 10) {
                            throw new Exception();
                        }
                    } catch (Exception var9) {
                        sender.sendMessage(ChatColor.RED + "Неверная фракция");
                        return true;
                    }

                    switch((faction = args[0]).hashCode()) {
                        case 48:
                            if (faction.equals("0")) {
                                FCBFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 49:
                            if (faction.equals("1")) {
                                PPSFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 50:
                            if (faction.equals("2")) {
                                AOFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 51:
                            if (faction.equals("3")) {
                                GIBDDFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 52:
                            if (faction.equals("4")) {
                                OBFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 53:
                            if (faction.equals("5")) {
                                SMIFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 54:
                            if (faction.equals("6")) {
                                ASHFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 55:
                            if (faction.equals("7")) {
                                MCHSFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 56:
                            if (faction.equals("8")) {
                                SEPFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 57:
                            if (faction.equals("9")) {
                                PIRFaction.invite(args[1].toLowerCase());
                            }
                            break;
                        case 1567:
                            if (faction.equals("10")) {
                                BATFaction.invite(args[1].toLowerCase());
                            }
                    }

                    sender.sendMessage(ChatColor.GREEN + "Игрок успешно приглашен во фракцию");
                    return true;
                }
            }
        } else {
            return false;
        }
    }
}