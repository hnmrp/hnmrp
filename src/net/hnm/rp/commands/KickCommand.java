package net.hnm.rp.commands;

import net.hnm.rp.main.HeavenRolePlay;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KickCommand implements CommandExecutor {
    private HeavenRolePlay plugin;

    public KickCommand(HeavenRolePlay plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length != 1) {
            return false;
        } else if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Команда может быть выполнена только игроком");
            return true;
        } else {
            Player p = (Player)sender;
            if (!this.plugin.getData().contains("players." + args[0].toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Игрок не находится в какой-либо фракции");
                return true;
            } else if (p.hasPermission("HeavenFactions.pfkick")) {
                this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                this.plugin.saveDataConfig();
                sender.sendMessage(ChatColor.GREEN + "Игрок успешно удален из фракции");
                return true;
            } else {
                String faction = this.plugin.getData().getString("players." + p.getName().toLowerCase()).split(" ")[0];
                String rank = this.plugin.getData().getString("players." + p.getName().toLowerCase()).split(" ")[1];
                String faction2 = this.plugin.getData().getString("players." + args[0].toLowerCase()).split(" ")[0];
                switch(faction.hashCode()) {
                    case 48:
                        if (faction.equals("0")) {
                            if (!rank.equals("8") && !rank.equals("9") || Integer.parseInt(faction2) >= 8) {
                                sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок находится в нелегальной фракции");
                                return true;
                            }

                            this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                            this.plugin.saveDataConfig();
                        }
                        break;
                    case 49:
                        if (!faction.equals("1")) {
                            break;
                        }

                        if (!rank.equals("8") && (!rank.equals("9") || !faction2.equals("1"))) {
                            sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                            return true;
                        }

                        this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                        this.plugin.saveDataConfig();
                        break;
                    case 50:
                        if (!faction.equals("2")) {
                            break;
                        }

                        if (!rank.equals("8") && (!rank.equals("9") || !faction2.equals("2"))) {
                            sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                            return true;
                        }

                        this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                        this.plugin.saveDataConfig();
                        break;
                    case 51:
                        if (faction.equals("3")) {
                            if (!rank.equals("8") && (!rank.equals("9") || !faction2.equals("3"))) {
                                sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                                return true;
                            }

                            this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                            this.plugin.saveDataConfig();
                        }
                        break;
                    case 52:
                        if (!faction.equals("4")) {
                            break;
                        }

                        if (!rank.equals("8") && (!rank.equals("9") || !faction2.equals("4"))) {
                            sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                            return true;
                        }

                        this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                        this.plugin.saveDataConfig();
                        break;
                    case 53:
                        if (!faction.equals("5")) {
                            break;
                        }

                        if (!rank.equals("8") && (!rank.equals("9") || !faction2.equals("5"))) {
                            sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                            return true;
                        }

                        this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                        this.plugin.saveDataConfig();
                        break;
                    case 54:
                        if (faction.equals("6")) {
                            if (!rank.equals("6") && (!rank.equals("7") || !faction2.equals("6"))) {
                                sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                                return true;
                            }

                            this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                            this.plugin.saveDataConfig();
                        }
                        break;
                    case 55:
                        if (faction.equals("7")) {
                            if (!rank.equals("7") && (!rank.equals("8") || !faction2.equals("7"))) {
                                sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                                return true;
                            }

                            this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                            this.plugin.saveDataConfig();
                        }
                        break;
                    case 56:
                        if (!faction.equals("8")) {
                            break;
                        }

                        if (!rank.equals("10") && (!rank.equals("11") || !faction2.equals("8"))) {
                            sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                            return true;
                        }

                        this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                        this.plugin.saveDataConfig();
                        break;
                    case 57:
                        if (faction.equals("9")) {
                            if (!rank.equals("8") && (!rank.equals("9") || !faction2.equals("9"))) {
                                sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                                return true;
                            }

                            this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                            this.plugin.saveDataConfig();
                        }
                        break;
                    case 1567:
                        if (faction.equals("10")) {
                            if (!rank.equals("8") && (!rank.equals("9") || !faction2.equals("10"))) {
                                sender.sendMessage(ChatColor.RED + "У вас слишком низкое звание для увольнения игроков или игрок не находится в вашей фракции");
                                return true;
                            }

                            this.plugin.getData().set("players." + args[0].toLowerCase(), (Object)null);
                            this.plugin.saveDataConfig();
                        }
                }

                sender.sendMessage(ChatColor.GREEN + "Игрок успешно уволен");
                return true;
            }
        }
    }
}