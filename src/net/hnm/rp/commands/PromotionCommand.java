package net.hnm.rp.commands;

import net.hnm.rp.main.HeavenRolePlay;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PromotionCommand implements CommandExecutor {
    private HeavenRolePlay plugin;

    public PromotionCommand(HeavenRolePlay plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length != 1) {
            return false;
        } else if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Команда может быть выполнена только игроком");
            return true;
        } else {
            Player p = (Player)sender;
            if (!this.plugin.getData().contains("players." + p.getName().toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Вы не находитесь в какой-либо фракции");
                return true;
            } else if (!this.plugin.getData().contains("players." + args[0].toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Игрок не находится в какой-либо фракции");
                return true;
            } else {
                String faction = this.plugin.getData().getString("players." + p.getName().toLowerCase()).split(" ")[0];
                String rank = this.plugin.getData().getString("players." + p.getName().toLowerCase()).split(" ")[1];
                String faction2 = this.plugin.getData().getString("players." + args[0].toLowerCase()).split(" ")[0];
                String rank2 = this.plugin.getData().getString("players." + args[0].toLowerCase()).split(" ")[1];
                if (!faction.equals(faction2)) {
                    sender.sendMessage(ChatColor.RED + "Игрок находится не в вашей фракции");
                    return true;
                } else if (Integer.parseInt(faction) >= 6 && Integer.parseInt(faction) <= 8) {
                    if (Integer.parseInt(faction) == 6) {
                        if ((rank.equals("6") || rank.equals("7")) && Integer.parseInt(rank2) != 7) {
                            this.plugin.getData().set("players." + args[0].toLowerCase(), faction + " " + (Integer.parseInt(rank2) + 1));
                            this.plugin.saveDataConfig();
                            p.sendMessage(ChatColor.GREEN + "Игрок повышен в звании");
                            return true;
                        } else {
                            p.sendMessage(ChatColor.RED + "У вас слишком низкое звание или у игрока максимальное звание во фракции");
                            return true;
                        }
                    } else if (Integer.parseInt(faction) == 7) {
                        if ((rank.equals("7") || rank.equals("8")) && Integer.parseInt(rank2) != 9) {
                            this.plugin.getData().set("players." + args[0].toLowerCase(), faction + " " + (Integer.parseInt(rank2) + 1));
                            this.plugin.saveDataConfig();
                            p.sendMessage(ChatColor.GREEN + "Игрок повышен в звании");
                            return true;
                        } else {
                            p.sendMessage(ChatColor.RED + "У вас слишком низкое звание или у игрока максимальное звание во фракции");
                            return true;
                        }
                    } else if (Integer.parseInt(faction) != 8) {
                        return false;
                    } else if ((rank.equals("10") || rank.equals("11")) && Integer.parseInt(rank2) != 11) {
                        this.plugin.getData().set("players." + args[0].toLowerCase(), faction + " " + (Integer.parseInt(rank2) + 1));
                        this.plugin.saveDataConfig();
                        p.sendMessage(ChatColor.GREEN + "Игрок повышен в звании");
                        return true;
                    } else {
                        p.sendMessage(ChatColor.RED + "У вас слишком низкое звание или у игрока максимальное звание во фракции");
                        return true;
                    }
                } else if ((rank.equals("8") || rank.equals("9")) && Integer.parseInt(rank2) != 9) {
                    this.plugin.getData().set("players." + args[0].toLowerCase(), faction + " " + (Integer.parseInt(rank2) + 1));
                    this.plugin.saveDataConfig();
                    p.sendMessage(ChatColor.GREEN + "Игрок повышен в звании");
                    return true;
                } else {
                    p.sendMessage(ChatColor.RED + "У вас слишком низкое звание или у игрока максимальное звание во фракции");
                    return true;
                }
            }
        }
    }
}
