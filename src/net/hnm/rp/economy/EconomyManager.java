package net.hnm.rp.economy;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import sun.jvm.hotspot.asm.Register;

public class EconomyManager {

    private static Economy economy;

    public static void init(){
        RegisteredServiceProvider<Economy> reg = Bukkit.getServicesManager().getRegistration(Economy.class);
        if(reg != null) economy = reg.getProvider();
    }


    public static boolean takeMoney(Player player, double amount){
        if(economy == null) return false;
        if(economy.getBalance(player) < amount) return false;
        return economy.withdrawPlayer(player, amount).transactionSuccess();
    }
}
