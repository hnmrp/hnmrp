package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class ASHFaction {
    public static String FactionName = "АШ";
    public static ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public ASHFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("Стажер");
        ranks.add("Консультант");
        ranks.add("Экзаменатор");
        ranks.add("Младший инструктор");
        ranks.add("Инструктор");
        ranks.add("Мененджер");
        ranks.add("Заместитель директора");
        ranks.add("Директор");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "6 0");
        plugin.saveDataConfig();
    }
}