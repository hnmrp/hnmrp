package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class BATFaction {
    public static final String FactionName = "Батыревская бригада";
    public static final ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public BATFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("Подросток");
        ranks.add("Охранник");
        ranks.add("Солдат");
        ranks.add("Закаленный");
        ranks.add("Решала");
        ranks.add("Надежный");
        ranks.add("Наемный убийца");
        ranks.add("Поставщик");
        ranks.add("Правая рука");
        ranks.add("Авторитет");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "10 0");
        plugin.saveDataConfig();
    }
}