package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class FCBFaction {
    public static final String FactionName = "ФСБ";
    public static final ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public FCBFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("Стажер");
        ranks.add("Младший агент");
        ranks.add("Агент");
        ranks.add("Старший агент");
        ranks.add("Специальный агент");
        ranks.add("Секретный агент");
        ranks.add("Укравляющий агентурой");
        ranks.add("Начальник СПКОО");
        ranks.add("Заместитель директора ФСБ");
        ranks.add("Директор ФСБ");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "0 0");
        plugin.saveDataConfig();
    }
}