package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;


public class GIBDDFaction {
    public static String FactionName = "ГИБДД";
    public static ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public GIBDDFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("Рядовой");
        ranks.add("Сержант");
        ranks.add("Старший сержант");
        ranks.add("Лейтенант");
        ranks.add("Старший лейтенант");
        ranks.add("Капитан");
        ranks.add("Майор");
        ranks.add("Подполковник");
        ranks.add("Полковник");
        ranks.add("Генерал");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "3 0");
        plugin.saveDataConfig();
    }
}