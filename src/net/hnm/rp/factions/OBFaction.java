package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class OBFaction {
    public static final String FactionName = "Областная больница";
    public static final ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public OBFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("Интерн");
        ranks.add("Младший работник");
        ranks.add("Старший работник");
        ranks.add("Участковый врач");
        ranks.add("Терапевт");
        ranks.add("Окулист");
        ranks.add("Хирург");
        ranks.add("Старший специалист");
        ranks.add("Заведующий отделением");
        ranks.add("Главный врач");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "4 0");
        plugin.saveDataConfig();
    }
}