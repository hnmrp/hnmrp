package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class PIRFaction {
    public static String FactionName = "Пираты";
    public static ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public PIRFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("Новобранец (Сын)");
        ranks.add("Солдат (Сын)");
        ranks.add("Солдат - штурмовик (Сын)");
        ranks.add("Солдат - пулеметчик (Сын)");
        ranks.add("Солдат - снайпер (Сын)");
        ranks.add("Солдат - техник (Сын)");
        ranks.add("Инструктор (Брат)");
        ranks.add("Старший инструктор (Брат)");
        ranks.add("Доверенное лицо (Старший брат)");
        ranks.add("Отец-генерал");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "9 0");
        plugin.saveDataConfig();
    }
}
