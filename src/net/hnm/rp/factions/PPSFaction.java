package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class PPSFaction {
    public static final String FactionName = "ППС";
    public static final ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public PPSFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("Рядовой");
        ranks.add("Сержант");
        ranks.add("Старший сержант");
        ranks.add("Лейтенант");
        ranks.add("Старший лейтенант");
        ranks.add("Капитан");
        ranks.add("Майор");
        ranks.add("Подполковник");
        ranks.add("Полковник");
        ranks.add("Генерал");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "1 0");
        plugin.saveDataConfig();
    }
}