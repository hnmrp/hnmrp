package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class SMIFaction {
    public static final String FactionName = "СМИ";
    public static final ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public SMIFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("Стажер");
        ranks.add("Редактор");
        ranks.add("Ведущий");
        ranks.add("Репортер");
        ranks.add("Звукооператор");
        ranks.add("Звукорежиссер");
        ranks.add("Корректор");
        ranks.add("Программный директор");
        ranks.add("Заместитель директора СМИ");
        ranks.add("Директор СМИ");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "5 0");
        plugin.saveDataConfig();
    }
}
