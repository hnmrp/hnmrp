package net.hnm.rp.interiors;

import net.hnm.rp.main.HeavenRolePlay;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.*;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.List;

public class FactionSign implements Listener {

    private World w;
    private double x, y, z;
    private int factions;

    @EventHandler
    public void onSignCreate(SignChangeEvent event){
        Player player = event.getPlayer();

        if(!player.hasPermission("HeavenFactions.admin")) return;
        if(!event.getLine(0).equalsIgnoreCase("[Офис]")) return;

        String name = event.getLine(1);
        String key = locToString(event.getBlock().getLocation());


        if(HeavenRolePlay.getInteriorsData().contains(name)) return;

        HeavenRolePlay.getInteriorsData().set(".Фракции" + factions + "." + ".Фракция", name);
        HeavenRolePlay.getInteriorsData().set(".Информация" + ".Координаты", key);
        HeavenRolePlay.getInteriorsData().set(".Информация" + ".World", "select");
        HeavenRolePlay.getInteriorsData().set(".Информация" + ".X", "select");
        HeavenRolePlay.getInteriorsData().set(".Информация" + ".Y", "select");
        HeavenRolePlay.getInteriorsData().set(".Информация" + ".Z", "select");
        HeavenRolePlay.saveDataConfig();

        event.setLine(3, "§1§lВойти");

        player.sendMessage("Табличка интерьера создана!");
    }

    @EventHandler
    public void onSignBreak(BlockBreakEvent event){
        Player player = event.getPlayer();

        if(!player.hasPermission("HeavenFactions.admin")) return;

        String key = locToString(event.getBlock().getLocation());
        Block breakedBlock = event.getBlock();
        BlockState state = breakedBlock.getState();
        if(state instanceof Sign) {
            String name = ((Sign) state).getLine(1);
            if(HeavenRolePlay.getInteriorsData().getString("Factions",name) != null){
                HeavenRolePlay.getInteriorsData().set(key, null);
                HeavenRolePlay.getInteriorsData().set(name + ".World", null);
                HeavenRolePlay.getInteriorsData().set(name + ".X", null);
                HeavenRolePlay.getInteriorsData().set(name + ".Y", null);
                HeavenRolePlay.getInteriorsData().set(name + ".Z", null);
                HeavenRolePlay.saveDataConfig();
                player.sendMessage("Табличка интерьера удалена!");
            }
        }
    }

    @EventHandler
    public void onSignClick(PlayerInteractEvent event){
        Player player = event.getPlayer();
        Block clickedBlock = event.getClickedBlock();
        BlockState state = clickedBlock.getState();
        String key = locToString(event.getClickedBlock().getLocation());
        if(event.getAction() != Action.RIGHT_CLICK_BLOCK) return;

        if(state instanceof Sign){
            Sign sign = (Sign) state;
            String line1 = sign.getLine(0);
            if(line1.equalsIgnoreCase("[Офис]")) {
                getInteriors(sign.getLine(1));
                Location loc = new Location(w, x, y, z);
                player.teleport(loc);
                player.sendMessage("Вы вошли в офис " + sign.getLine(1));
            }
        }
    }

    private String locToString(Location location){
        return location.getWorld().getName() + ", " + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ();
    }

    private void getInteriors(String faction){
        w = Bukkit.getWorld(HeavenRolePlay.getInteriorsData().getString((faction + ".World")));
        x = HeavenRolePlay.getInteriorsData().getDouble(faction + ".X");
        y = HeavenRolePlay.getInteriorsData().getDouble(faction + ".Y");
        z = HeavenRolePlay.getInteriorsData().getDouble(faction + ".Z");
    }
}
