package net.hnm.rp.interiors;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.hnm.rp.economy.EconomyManager;
import net.hnm.rp.main.HeavenRolePlay;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class HouseSign implements Listener {

    private Sign sign;

    @EventHandler
    public void onSignCreate(SignChangeEvent event){
        Player player = event.getPlayer();

        if(!player.hasPermission("HeavenFactions.admin")) return;
        if(!event.getLine(0).equalsIgnoreCase("[House]")) return;

        String name = event.getLine(1);
        String key = locToString(event.getBlock().getLocation());
        double price;
        try {
            price = Double.parseDouble(event.getLine(2));
        } catch (NumberFormatException exception) {
            event.setLine(3, "&4Invalid price");
            return;
        }

        if(price < 0){
            event.setLine(3, "&4Invalid price");
            return;
        }

        HeavenRolePlay.getInteriorsData().get(".Houses");
        HeavenRolePlay.getInteriorsData().set(key + ".House", name);
        HeavenRolePlay.getInteriorsData().set(key + ".Price", price);
        HeavenRolePlay.getInteriorsData().set(name + ".World", "select");
        HeavenRolePlay.getInteriorsData().set(name + ".X", "select");
        HeavenRolePlay.getInteriorsData().set(name + ".Y", "select");
        HeavenRolePlay.getInteriorsData().set(name + ".Z", "select");
        HeavenRolePlay.getInteriorsData().set(".Players", player.getName());
        HeavenRolePlay.saveDataConfig();

        event.setLine(0, ChatColor.GREEN + "Купить дом");
        event.setLine(1, ChatColor.GREEN + "Цена: " + price);
        event.setLine(2, ChatColor.WHITE + name);

        player.sendMessage("Табличка интерьера создана!");
    }

    @EventHandler
    public void onSignBreak(BlockBreakEvent event){
        Player player = event.getPlayer();

        if(!player.hasPermission("HeavenFactions.admin")) return;

        String key = locToString(event.getBlock().getLocation());
        Block breakedBlock = event.getBlock();
        BlockState state = breakedBlock.getState();
        if(state instanceof Sign) {
            String name = ((Sign) state).getLine(1);
            if(HeavenRolePlay.getInteriorsData().getString(".Houses", key + ".House") != null){
                HeavenRolePlay.getInteriorsData().set(key, null);
                HeavenRolePlay.getInteriorsData().set(key, null);
                HeavenRolePlay.getInteriorsData().set(name, null);
                HeavenRolePlay.getInteriorsData().set(name, null);
                HeavenRolePlay.getInteriorsData().set(name, null);
                HeavenRolePlay.getInteriorsData().set(name , null);
                HeavenRolePlay.getInteriorsData().set(name, null);
                HeavenRolePlay.saveDataConfig();
                player.sendMessage("Табличка интерьера удалена!");
            }
        }
    }

    @EventHandler
    public void onSignClick(PlayerInteractEvent event){
        Player player = event.getPlayer();
        Block clickedBlock = event.getClickedBlock();
        BlockState state = clickedBlock.getState();
        String key = locToString(event.getClickedBlock().getLocation());
        String region = HeavenRolePlay.getInteriorsData().getString("Houses", key + ".House");
        double price = HeavenRolePlay.getInteriorsData().getDouble(key + ".Price");
        if(region == null) return;
        if(price == 0) return;

        RegionManager manager = WorldGuardPlugin.inst().getRegionManager(event.getClickedBlock().getWorld());
        ProtectedRegion pregion = manager.getRegion(region);

        if(pregion == null){
            player.sendMessage("Region not found!");
            return;
        }

        if(pregion.getOwners().size() > 0 || pregion.getMembers().size() > 0){
            player.sendMessage("Region was alredy sold!");
            return;
        }

        if(!EconomyManager.takeMoney(player, price)) {
            player.sendMessage("Region was alredy sold!");
            return;
        }

        pregion.getOwners().addPlayer(player.getName());

        Sign sign = (Sign) state;
        sign.setLine(1, "§4§lПродано!");
        sign.setLine(3, "Владелец: " + pregion.getOwners().getPlayers().toString());
        sign.update();
    }

    private String locToString(Location location){
        return location.getWorld().getName() + ", " + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ();
    }
}
