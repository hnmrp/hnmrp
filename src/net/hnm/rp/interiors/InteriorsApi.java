package net.hnm.rp.interiors;

import net.hnm.rp.main.HeavenRolePlay;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Set;

public class InteriorsApi {

    public static double getHousePrice(String region){
        FileConfiguration fc = HeavenRolePlay.getInteriorsData();
        Set<String> keys = fc.getKeys(false);
        if(keys == null) return 0;
        for(String k : keys){
            if(region.equalsIgnoreCase(fc.getString(".Houses", k + ".House"))){
                return fc.getDouble(k + ".Price");
            }
        }
        return 0;
    }
}
