package net.hnm.rp.main;

import net.hnm.rp.factions.*;
import net.hnm.rp.commands.*;
import java.io.File;
import java.io.IOException;

import net.hnm.rp.interiors.FactionSign;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class HeavenRolePlay extends JavaPlugin {
    private static YamlConfiguration data, interiorsData;
    private static File dataFile, interiorFile;

    public void onEnable() {
        getDataFolder().mkdirs();
        this.dataFile = new File(getDataFolder() + File.separator + "players.yml");
        this.interiorFile = new File(getDataFolder() + File.separator + "interiors.yml");
        if (!this.dataFile.exists() || !this.interiorFile.exists()) {
            try {
                this.dataFile.createNewFile();
                this.interiorFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        getServer().getPluginManager().registerEvents(new FactionSign(), this);
        getServer().getPluginManager().registerEvents(new MySQLManager(), this);
        initializeFactions();
        this.data = YamlConfiguration.loadConfiguration(this.dataFile);
        this.interiorsData = YamlConfiguration.loadConfiguration(this.interiorFile);
        getCommand("pfinvite").setExecutor(new InviteCommand(this));
        getCommand("pfkick").setExecutor(new KickCommand(this));
        getCommand("promotion").setExecutor(new PromotionCommand(this));
        getCommand("reduction").setExecutor(new ReductionCommand(this));
        getCommand("r").setExecutor(new RChat(this));
        getCommand("rb").setExecutor(new RBChat(this));
        getCommand("d").setExecutor(new DChat(this));
        getCommand("db").setExecutor(new DBChat(this));
        getCommand("pfspy").setExecutor(new SpyCommand());
        getCommand("pfinfo").setExecutor(new InfoCommand(this));
        getCommand("pfplayers").setExecutor(new ListCommand(this));
        getLogger().info("Plugin was enabled");
    }


    public void onDisable() {
        getLogger().info("Plugin was disabled");
    }


    public YamlConfiguration getData() {
        return this.data;
    }

    public static YamlConfiguration getInteriorsData() {
        return interiorsData;
    }


    public File getDataFile() {
        return this.dataFile;
    }

    public static File getInteriorsDataFile() {
        return interiorFile;
    }

    private void initializeFactions() {
        AOFaction f1 = new AOFaction(this);
        ASHFaction f2 = new ASHFaction(this);
        BATFaction f3 = new BATFaction(this);
        FCBFaction f4 = new FCBFaction(this);
        GIBDDFaction f5 = new GIBDDFaction(this);
        MCHSFaction f6 = new MCHSFaction(this);
        OBFaction f7 = new OBFaction(this);
        PIRFaction f8 = new PIRFaction(this);
        PPSFaction f9 = new PPSFaction(this);
        SEPFaction f11 = new SEPFaction(this);
        SMIFaction f12 = new SMIFaction(this);
    }

    public static void saveDataConfig() {
        try {
            data.save(dataFile);
            interiorsData.save(interiorFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}